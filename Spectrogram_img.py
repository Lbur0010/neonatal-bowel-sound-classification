from python_speech_features import mfcc
import librosa
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import resample
import librosa.display
import seaborn as sns
import time
import scipy as sp
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from scipy.stats import ttest_ind, mannwhitneyu
from sklearn.metrics import roc_auc_score
from sklearn.metrics import recall_score
import tensorflow as tf
from keras.utils import np_utils
from tensorflow.keras.preprocessing import sequence
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Embedding
from tensorflow.keras.layers import LSTM
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.layers import Input, Flatten, Dropout, Activation,Reshape
from tensorflow.keras.layers import Conv1D, MaxPooling1D, AveragePooling1D, LSTM
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ModelCheckpoint
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.layers import BatchNormalization, Lambda, Bidirectional
from tensorflow.keras.layers import Conv2D, MaxPooling2D,GlobalAveragePooling2D
from hmmlearn.hmm import MultinomialHMM
from tensorflow.keras.callbacks import EarlyStopping
import os

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.compat.v1.Session(config=config)

# samples = 0  ## ???#mention the samples
# path = 'C:/Users/Sam/Desktop/OneDrive/Monash @Dr. Faezeh Marzbanrad/Workspace/Bowel Sound Classification/Feature Engineering Method/'
# path = 'C:/Users/Chiru/PycharmProjects/Neonatal/'
path = 'G:/Neonatal Internship/neonatal database/fmarzbanrad-bowel-sound-classification-b9b7bb493a73'


def read_annotation(file):
    df = pd.read_table(file, sep='\t', header=None)
    ann = {}
    segs_0 = []
    segs_1 = []

    # extract segments with peristalticSound
    for idx, label in enumerate(df[8]):
        if label.strip().find('peristalticSound') != -1:
            segs_1.append([df.loc[idx, 3], df.loc[idx, 5]])
        else:
            continue
    ann[1] = segs_1

    # extract segments without peristalticSound
    last = 0.00
    for segment in segs_1:
        if segment[0] - last > 3:
            segs_0.append([last, segment[0]])
        last = segment[1]
    ann[0] = segs_0

    return ann


data = []
features = []
labels = []
subjects = []
segLen = 6
overlap = 0.1

# Retrieve file name from the list

# Get data

samples = os.listdir(path + '/data with regularized annotations')

s = []

for f in samples:
    k = f.split('.')
    print(str(k[0]) + '\n')
    s.append(k[0])

samples = list(set(s))

for sample in samples:
    annotation = read_annotation(path + '/data with regularized annotations/' + sample + '.txt')
    wave, sr = librosa.load(path + '/data with regularized annotations/' + sample + '.wav', sr=None)
    for label in range(2):
        for values in annotation[label]:
            if values[1] - values[0] < segLen:
                continue
            else:
                offset = 0
                while True:
                    start = int((values[0] + offset) * sr)
                    end = int((values[0] + offset + segLen) * sr)
                    if values[0] + offset + segLen > values[1]:
                        break
                    seg = wave[start: end]
                    labels.append(label)
                    subjects.append(sample)
                    data.append(seg)
                    offset += overlap
                    
                    tensor = librosa.stft(seg)
                    tensor, _ = librosa.magphase(tensor)
                    tensor = librosa.feature.melspectrogram(S=tensor, sr=sr)
                    tensor = librosa.amplitude_to_db(abs(tensor))
                    print(tensor.shape)
                    plt.imshow(tensor)
                    plt.show()
                    tensor = np.reshape(tensor,(tensor.shape[0],tensor.shape[1],))
                    features.append(tensor)  # 2D spectrogram features
# pd_features = pd.DataFrame(features)
# pd_features.to_csv('segments.csv')

# pd_features.head()

# Test 2D MFCC in features list

base_model = tf.keras.applications.resnet50.ResNet50(include_top=False, weights='imagenet', input_shape=(features[0].shape[0], features[0].shape[1], 3))
base_learning_rate = 0.0001

def mcnn():
    input_layer = tf.keras.layers.Input((features[0].shape[0], features[0].shape[1], 3))
    x = Conv2D(filters=128, kernel_size=(2, 2), padding='same', activation='relu')(input_layer)
    x = Dropout(0.1)(x)
    x = BatchNormalization()(x)
    x = Conv2D(filters=64, kernel_size=(2, 2), padding='same', activation='relu')(x)
    x = Dropout(0.1)(x)
    x = BatchNormalization()(x)
    # x = Conv2D(filters=128, kernel_size=(2, 2), padding='same', activation='relu')(x)
    # x = Dropout(0.1)(x)
    # x = BatchNormalization()(x)
    x = MaxPooling2D(pool_size=(2, 2), strides=2, padding='same')(x)
    x= GlobalAveragePooling2D()(x)
    print(x.shape)
    #x=GlobalAveragePooling2D()(x)

    y = Conv2D(filters=128, kernel_size=(4, 4), padding='same', activation='relu')(input_layer)
    y = Dropout(0.1)(y)
    y = BatchNormalization()(y)
    y = Conv2D(filters=64, kernel_size=(4, 4), padding='same', activation='relu')(y)
    y = Dropout(0.1)(y)
    y = BatchNormalization()(y)
    # y = Conv2D(filters=128, kernel_size=(4, 4), padding='same', activation='relu')(y)
    # y = Dropout(0.1)(y)
    # y = BatchNormalization()(y)
    y = MaxPooling2D(pool_size=(2, 2))(y)
    y=GlobalAveragePooling2D()(y)
    print(y.shape)
    #y=GlobalAveragePooling2D(y)

    z = Conv2D(filters=128, kernel_size=(8, 8), padding='same', activation='relu')(input_layer)
    z = Dropout(0.1)(z)
    z = BatchNormalization()(z)
    z = Conv2D(filters=64, kernel_size=(8, 8), padding='same', activation='relu')(z)
    z = Dropout(0.1)(z)
    z = BatchNormalization()(z)
    # z = Conv2D(filters=128, kernel_size=(8, 8), padding='same', activation='relu')(z)
    # z = Dropout(0.1)(z)
    # z = BatchNormalization()(z)
    z = MaxPooling2D(pool_size=(2, 2))(z)
    z=GlobalAveragePooling2D()(z)
    print(z.shape)
   # z=GlobalAveragePooling2D()(z)

    a = Conv2D(filters=128, kernel_size=(16, 16), padding='same', activation='relu')(input_layer)
    a = Dropout(0.1)(a)
    a = BatchNormalization()(a)
    a = Conv2D(filters=64, kernel_size=(16, 16), padding='same', activation='relu')(a)
    a = Dropout(0.1)(a)
    a = BatchNormalization()(a)
    # a = Conv2D(filters=128, kernel_size=(16, 16), padding='same', activation='relu')(a)
    # a = Dropout(0.1)(a)
    # a = BatchNormalization()(a)
    a = MaxPooling2D(pool_size=(2, 2))(a)
    a=GlobalAveragePooling2D()(a)
    print(a.shape)
    #a=GlobalAveragePooling2D()(a)

    #add_layer = keras.layers.add([x, y, z, a])
    # output_layer = Flatten()(add_layer)
    output_layer=tf.keras.layers.concatenate([x,y,z,a])
    output_layer = Dense(16)(output_layer)
    output_layer = Dense(2)(output_layer)
    output_layer = Activation('softmax')(output_layer)
    model = tf.keras.models.Model(inputs=input_layer, outputs=output_layer)
    # opt = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)
    opt = tf.keras.optimizers.RMSprop(learning_rate=0.00001, decay=1e-6)
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    return model

def pretrained_MCNN():
    global_average_layer = tf.keras.layers.GlobalAveragePooling2D()
    prediction_layer = tf.keras.layers.Dense(2)

    inputs = tf.keras.Input(shape=(features[0].shape[0], features[0].shape[1], 3))
    x = base_model(inputs, training=False)
    x = global_average_layer(x)
    x = tf.keras.layers.Dropout(0.2)(x)
    outputs = prediction_layer(x)
    model = tf.keras.Model(inputs, outputs)

    model.compile(optimizer=tf.keras.optimizers.RMSprop(learning_rate=base_learning_rate), loss='categorical_crossentropy', metrics=['accuracy'])
    return model

def pretrained_MCNN_train(x_train, y_train, epochs, fine_tune_epochs, test_sub):
    base_model.trainable = False

    callbacks = [
        tf.keras.callbacks.ModelCheckpoint(
            filepath = path + '/spectrogram_pretrained/' + test_sub + '.h5',
            save_best_only=True,
            monitor="val_loss",
            verbose=1,
        )
    ]

    model = pretrained_MCNN()

    history = model.fit(x_train, y_train, epochs=epochs, validation_split=0.3, verbose=True, callbacks=callbacks)
    #plot_history(history)

    model.load_weights(path + '/spectrogram_pretrained/' + test_sub + '.h5')

    #base_model.trainable = True
    #fine_tune = int(len(base_model.layers) * 0.66666)
    #for layer in base_model.layers[:fine_tune]:
        #layer.trainable = False
    for layer in model.layers:
        layer.trainable = False
    model.save_weights(path + '/spectrogram_pretrained/' + test_sub + '.h5')

    callbacks_fine = [
        tf.keras.callbacks.ModelCheckpoint(
            filepath = path + '/spectrogram_pretrained/' + test_sub + '_fine.h5',
            save_best_only=True,
            monitor="val_loss",
            verbose=1,
        )
    ]

    #model.compile(loss='categorical_crossentropy', optimizer=tf.keras.optimizers.RMSprop(learning_rate=base_learning_rate/10), metrics=['accuracy'])
    #total_epochs = epochs + fine_tune_epochs

    #history_fine = model.fit(x_train, y_train, epochs=total_epochs, initial_epoch=history.epoch[-1], validation_split=0.3, verbose=True, callbacks=callbacks_fine)
    #plot_history(history_fine)

# Train the model and save it in the directory for reproducibility

def plot_history(history):
    print(history.history.keys())
    #  "Accuracy"
    plt.figure(0)
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()
    # "Loss"
    plt.figure(1)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Validation'], loc='upper left')
    plt.show()


# Mention the parameters for ensembled model


# def ensembled_model(path, test_sub):
#     input_layer = keras.layers.Input((24, 1))
#     m1 = channel1_model()
#     m1.load_weights(path + 'c1/' + test_sub + '.h5')
#     m2 = channel2_model()
#     m2.load_weights(path + 'c2/' + test_sub + '.h5')
#     m3 = channel3_model()
#     m3.load_weights(path + 'c3/' + test_sub + '.h5')
#     m4 = channel4_model()
#     m4.load_weights(path + 'c4/' + test_sub + '.h5')
#
#     # Freeze the layers
#     # for layer in m1.layers:
#     #     layer.name = layer.name + 'first'
#     #     layer.trainable = False
#
#     for i, layer in enumerate(m1.layers):
#         layer.name = str(i) + 'first'
#         layer.trainable = False
#
#     # for layer in m2.layers:
#     #     layer.name = layer.name + 'second'
#     #     layer.trainable = False
#     for i, layer in enumerate(m2.layers):
#         layer.name = str(i) + 'second'
#         layer.trainable = False
#
#     # for layer in m3.layers:
#     #     layer.name = layer.name + 'third'
#     #     layer.trainable = False
#     for i, layer in enumerate(m3.layers):
#         layer.name = str(i) + 'third'
#         layer.trainable = False
#
#     # for layer in m4.layers:
#     #     layer.name = layer.name + 'fourth'
#     #     layer.trainable = False
#     for i, layer in enumerate(m4.layers):
#         layer.name = str(i) + 'fourth'
#         layer.trainable = False
#
#     # extract intermediate layer from pre-trained model
#
#     # print(m1.summary())
#     # print(m2.summary())
#     # print(m3.summary())
#     # print(m4.summary())
#
#     # print('Ensembled model \n')
#     m1_output = m1.get_layer('10first').output
#     m2_output = m2.get_layer('10second').output
#     m3_output = m3.get_layer('10third').output
#     m4_output = m4.get_layer('10fourth').output
#
#     # print(m1_output.shape)
#     # print(m2_output.shape)
#     # print(m3_output.shape)
#     # print(m4_output.shape)
#
#     # truncated DL model
#     print('Individual models\n')
#     m1_new = keras.models.Model(inputs=m1.input, outputs=m1_output)
#     m2_new = keras.models.Model(inputs=m2.input, outputs=m2_output)
#     m3_new = keras.models.Model(inputs=m3.input, outputs=m3_output)
#     m4_new = keras.models.Model(inputs=m4.input, outputs=m4_output)
#
#     # extract the intermediate layer from the truncated model
#     # print(m1_new.summary())
#     # print(m2_new.summary())
#     # print(m3_new.summary())
#     # print(m4_new.summary())
#
#     m1output = m1_new(input_layer)
#     m2output = m2_new(input_layer)
#     m3output = m3_new(input_layer)
#     m4output = m4_new(input_layer)
#
#     # add the intermediate layers
#
#     add_layer = keras.layers.add([m1output, m2output, m3output, m4output])
#
#     output_layer = Flatten()(add_layer)
#     output_layer = Dense(16)(output_layer)
#     output_layer = Dense(2)(output_layer)
#     output_layer = Activation('softmax')(output_layer)
#     model = keras.models.Model(inputs=input_layer, outputs=output_layer)
#     opt = keras.optimizers.rmsprop(lr=0.00001, decay=1e-6)
#     model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
#     return model

k = 0
for test_sub in samples:
    temp_test_sub = test_sub
    print('Test_subject: ', test_sub)
    print(str(k) + '\n')
    k = k + 1
    train = []
    train_true = []
    train_lb = []
    test = []
    test_lb = []

    for idx, sub in enumerate(subjects):
        if sub == test_sub:
            test.append(features[idx])
            test_lb.append(labels[idx])
        else:
            # train.append(features[idx])
            # train_lb.append(labels[idx])
            if labels[idx] == 1:
                train_true.append(features[idx])
            else:
                train.append(features[idx])
                train_lb.append(labels[idx])
    print(len(train_true))
    print(len(train_lb))
    train += resample(train_true, replace=False, n_samples=len(train_lb))  # Down sampling
    train_lb += [1 for i in train_lb]
    print(len(train_lb))

    test_lb.append(0)
    test_lb.append(1)
    x_train = np.array(train)
    y_train = np.array(train_lb)
    x_test = np.array(test)
    y_test = np.array(test_lb)

    lb = LabelEncoder()

    y_train = np_utils.to_categorical(lb.fit_transform(y_train))
    y_test = np_utils.to_categorical(lb.fit_transform(y_test))[:-2]

    x_traincnn = np.expand_dims(x_train, axis=3)
    x_testcnn = np.expand_dims(x_test, axis=3)
    print(x_traincnn.shape)
    x_traincnn = tf.image.grayscale_to_rgb(tf.convert_to_tensor(x_traincnn))
    print(x_traincnn.shape)
    # model = mcnn()
    # model=channel1_model()
    # model = channel2_model()
    # model = channel3_model()
    # model = channel4_model()
    # model = ensembled_model(path, test_sub)
    #model = mcnn()
    # model=cnn_lstm()
    # Train model here with training dataset and use it for training and testing. #7:3, batch=32--marginal increment
    '''
    callbacks = [
        tf.keras.callbacks.ModelCheckpoint(
            filepath = path + '/spectrogram_trained/' + test_sub + '.h5',
            save_best_only=True,
            monitor="val_loss",
            verbose=1,
        )
    ]
    

    history = model.fit(x_traincnn, y_train, epochs=50, batch_size=8, callbacks=callbacks, validation_split=0.3,
                        verbose=True)
    plot_history(history)
    '''
    pretrained_MCNN_train(x_train=x_traincnn, y_train=y_train, epochs=100, fine_tune_epochs=0, test_sub=test_sub)

    #model.save_weights(path + '/spectrogram_trained/' + test_sub + '.h5')
    # model.save_weights(path + 'pre_trained_models/' + test_sub + '.h5')
    # model.save_weights(path + 'c1/' + test_sub + '.h5')
    # model.save_weights(path + 'c2/' + test_sub + '.h5')
    # model.save_weights(path + 'c3/' + test_sub + '.h5')
    # model.save_weights(path + 'c4/' + test_sub + '.h5')
    # model.save_weights(path + 'lstm/' + test_sub + '.h5')
    # model.save_weights(path+'cnn_lstm/'+test_sub+'.h5')


# Load models and evaluate the proposed methods
result = np.array([[0, 0], [0, 0]])
groundTruth = []
overallPrediction = []
probaPredictions = {}
for test_sub in samples:
    #print('Test_subject: ', temp_test_sub)
    #test_sub = temp_test_sub
    train = []
    train_true = []
    train_lb = []
    test = []
    test_lb = []

    for idx, sub in enumerate(subjects):
        if sub == test_sub:
            test.append(features[idx])
            test_lb.append(labels[idx])
        else:
            # train.append(features[idx])
            # train_lb.append(labels[idx])
            if labels[idx] == 1:
                train_true.append(features[idx])
            else:
                train.append(features[idx])
                train_lb.append(labels[idx])
    train += resample(train_true, replace=False, n_samples=len(train_lb))  # Downsampling
    train_lb += [1 for i in train_lb]

    test_lb.append(0)
    test_lb.append(1)
    x_train = np.array(train)
    y_train = np.array(train_lb)
    x_test = np.array(test)
    y_test = np.array(test_lb)

    lb = LabelEncoder()

    y_train = np_utils.to_categorical(lb.fit_transform(y_train))
    y_test = np_utils.to_categorical(lb.fit_transform(y_test))[:-2]

    x_traincnn = np.expand_dims(x_train, axis=3)
    x_testcnn = np.expand_dims(x_test, axis=3)
    x_testcnn = tf.image.grayscale_to_rgb(tf.convert_to_tensor(x_testcnn))
    # save_path = 'trained model/'
    # model = ensembled_model(path, test_sub)
    # model.load_weights(path + 'e/' + test_sub + '.h5')

    # model = channel1_model()
    # model.load_weights(path + 'c1/' + test_sub + '.h5')

    # model = channel2_model()
    # model.load_weights(path + 'c2/' + test_sub + '.h5')

    # model = channel3_model()
    # model.load_weights(path + 'c3/' + test_sub + '.h5')

    model = pretrained_MCNN()
    model.load_weights(path + '/spectrogram_pretrained/' + test_sub + '.h5')

    prediction = model.predict(x_testcnn)
    probaPredictions[test_sub] = prediction.copy()
    overallPrediction += list(prediction.argmax(axis=1))
    groundTruth += list(y_test.argmax(axis=1))
    cm = confusion_matrix(
        y_test.argmax(axis=1), prediction.argmax(axis=1), labels=[0, 1]
    )
    result += cm


print('Final result:', result)
print('Acc: ', accuracy_score(groundTruth, overallPrediction))
print(classification_report(groundTruth, overallPrediction))

##############Descision fusion code#####################

# Load models and evaluate the proposed methods


###################end of decision fusion code

from hsmmlearn.hsmm import GaussianHSMM
from hsmmlearn.hsmm import HSMMModel

from scipy.stats import laplace

from hsmmlearn.emissions import AbstractEmissions, MultinomialEmissions


class LaplaceEmissions(AbstractEmissions):
    dtype = np.float64

    def __init__(self, means, scales):
        self.means = means
        self.scales = scales

    def likelihood(self, obs):
        obs = np.squeeze(obs)
        return laplace.pdf(obs,
                           loc=self.means[:, np.newaxis],
                           scale=self.scales[:, np.newaxis])

    def sample_for_state(self, state, size=None):
        return laplace.rvs(self.means[state], self.scales[state], size)


def hsmm_get_transition_matrix(test_sub, sub_labels, samples):
    tm_cnt = np.zeros((2, 2), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels[0] == temp_sub][1])
        for idx in range(len(sub_seq) - 1):
            if sub_seq[idx] == sub_seq[idx + 1]:
                continue
            tm_cnt[sub_seq[idx], sub_seq[idx + 1]] += 1
    tm = np.zeros((2, 2), dtype=float)
    for i in range(2):
        for j in range(2):
            tm[i, j] = tm_cnt[i, j] / np.sum(tm_cnt[i])
    return tm


def hsmm_get_emission_matrix(train_cm):
    em = np.zeros((2, 2), dtype=float)
    for i in range(2):
        for j in range(2):
            em[i, j] = train_cm[j, i] / np.sum(train_cm[j])
    return em


def hsmm_get_duration(test_sub, sub_labels, samples):
    dr_cnt = np.zeros((2, 1000), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels[0] == temp_sub][1])
        idx = 0
        pre_state = 0
        cnt = 0
        while idx < len(sub_seq):
            if sub_seq[idx] == pre_state:
                cnt += 1
            else:
                if cnt != 0:
                    dr_cnt[pre_state, cnt - 1] += 1
                pre_state = sub_seq[idx]
                cnt = 1
            idx += 1
        if cnt != 0:
            dr_cnt[pre_state, cnt - 1] += 1
    dr = np.zeros((2, 1000), dtype=float)
    for i in range(2):
        dr[i] = dr_cnt[i] / np.sum(dr_cnt[i])
    return dr


def new_hsmm_get_duration(test_sub, sub_labels, samples):
    test_len = 600
    dr_cnt = np.ones((2, test_len), dtype=float)
    for temp_sub in samples:
        if temp_sub == test_sub:
            continue

        sub_seq = list(sub_labels[sub_labels[0] == temp_sub][1])
        idx = 0
        pre_state = 0
        cnt = 0
        while idx < len(sub_seq):
            if sub_seq[idx] == pre_state:
                cnt += 1
            else:
                if cnt != 0:
                    if cnt > test_len:
                        cnt = test_len
                    dr_cnt[pre_state, cnt - 1] += 100
                pre_state = sub_seq[idx]
                cnt = 1
            idx += 1
        if cnt != 0:
            if cnt > test_len:
                cnt = test_len
            dr_cnt[pre_state, cnt - 1] += 100
    dr = np.zeros((2, test_len), dtype=float)
    for i in range(2):
        dr[i] = dr_cnt[i] / np.sum(dr_cnt[i])
    return dr


def hsmm_get_init_probability(samples, test_sub):
    ini_prob = []
    cnt0 = 0
    cnt1 = 0
    for test_sub in samples:
        test_lb = []
        for idx, sub in enumerate(subjects):
            if sub == test_sub:
                test_lb.append(labels[idx])
        ini_prob.append(test_lb[0])
        if test_lb[0] == 0:
            cnt0 += 1
        else:
            cnt1 += 1
    return np.array([cnt0 / 49, cnt1 / 49])


overallPrediction = {}
newOverallPrediction = {}
for s in np.linspace(0.1, 5.0, num=50):
    overallPrediction[str(s)] = []
    newOverallPrediction[str(s)] = []
    for test_sub in samples:
        prediction = probaPredictions[test_sub]
        overallPrediction[str(s)] += list(prediction.argmax(axis=1))
        sub_labels = pd.concat([pd.Series(subjects), pd.Series(labels)], axis=1)

        tm = np.array([
            [0.001, 0.999],
            [0.999, 0.001]
        ])

        means = np.array([0.0, 1.0])
        scales = np.array([s, s])
        # scales = np.array([1.5, 1.5])
        dm = new_hsmm_get_duration(test_sub, sub_labels, samples)
        # dm = hsmm_get_duration(test_sub, sub_labels, samples)
        st = hsmm_get_init_probability(samples, test_sub)

        # h1 = GaussianHSMM(means, scales, dm, tm, st)
        h1 = HSMMModel(LaplaceEmissions(means, scales), dm, tm, st)
        # h1 = HSMMModel(MultinomialEmissions(hsmm_get_emission_matrix(train_cm)), dm, tm, st)

        # temp_observations = prediction.argmax(axis=1).reshape(-1,)
        temp_observations = prediction[:, 1].reshape(-1, )
        # observations = np.zeros_like(temp_observations, dtype='float64')
        observations = np.zeros_like(temp_observations, dtype='float64')
        for idx, sta in enumerate(temp_observations):
            observations[idx] = sta

        new_prediction = h1.decode(observations)

        newOverallPrediction[str(s)] += list(new_prediction)

x = []
y1 = []
y2 = []

for s in np.linspace(0.1, 5.0, num=50):
    x.append(s)
    y1.append(accuracy_score(groundTruth, newOverallPrediction[str(s)]))
    y2.append(roc_auc_score(groundTruth, newOverallPrediction[str(s)]))
    print(s)
    print(roc_auc_score(groundTruth, newOverallPrediction[str(s)]))
    print(accuracy_score(groundTruth, newOverallPrediction[str(s)]))
    print()

s = 5.0
# s = 3.9 #4.7  # 4.1
print('Before Refinement: ')
print(classification_report(groundTruth, overallPrediction[str(s)], digits=4))
print('After Refinement: ')
print(classification_report(groundTruth, newOverallPrediction[str(s)], digits=4))
print()
print('Before Refinement: ')
print('ACC: ', accuracy_score(groundTruth, overallPrediction[str(s)]))
print('After Refinement: ')
print('ACC: ', accuracy_score(groundTruth, newOverallPrediction[str(s)]))
print()
print('Before Refinement: ')
print('AUC: ', roc_auc_score(groundTruth, overallPrediction[str(s)]))
print('After Refinement: ')
print('AUC: ', roc_auc_score(groundTruth, newOverallPrediction[str(s)]))

a = pd.DataFrame({'x': x, 'y1': y1, 'y2': y2})
import matplotlib.pyplot as plt
import seaborn as sns

fig = plt.figure(dpi=100, figsize=(10, 5))
plt.rc('font', family='STFangsong')
sns.set(style="darkgrid")

plt.ylim(0.8, 0.9)

sns.scatterplot(x="x",
                y="y1",
                data=a, label='ACC')
sns.lineplot(x="x",
             y="y1",
             data=a)

sns.scatterplot(x="x",
                y="y2",
                data=a, label='AUC')
sns.lineplot(x="x",
             y="y2",
             data=a)

plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.xlabel('Standard deviation', fontdict={'size': 12})
plt.ylabel('Value', fontdict={'size': 15})
plt.legend(loc='upper left', labels=['ACC', 'AUC'], fontsize=12)
plt.savefig('hsmm parameters.png')

result = np.array([[0, 0], [0, 0]])
groundTruth = []
conventional_overallPrediction = []
conventional_newOverallPrediction = []

for test_sub in samples:
    print('Test_subject: ', test_sub)
    train = []
    train_true = []
    train_lb = []
    test = []
    test_lb = []

    for idx, sub in enumerate(subjects):
        if sub == test_sub:
            test.append(features[idx])
            test_lb.append(labels[idx])
        else:
            # train.append(features[idx])
            # train_lb.append(num_labels[idx])
            if labels[idx] == 1:
                train_true.append(features[idx])
            else:
                train.append(features[idx])
                train_lb.append(labels[idx])
    train += resample(train_true, replace=False, n_samples=len(train_lb))
    train_lb += [1 for i in train_lb]

    test_lb.append(0)
    test_lb.append(1)
    x_train = np.array(train)
    y_train = np.array(train_lb)
    x_test = np.array(test)
    y_test = np.array(test_lb)

    lb = LabelEncoder()

    y_train = np_utils.to_categorical(lb.fit_transform(y_train))
    y_test = np_utils.to_categorical(lb.fit_transform(y_test))[:-2]

    x_traincnn = np.expand_dims(x_train, axis=-1)
    x_testcnn = np.expand_dims(x_test, axis=-1)

    # save_path = 'trained_model/'
    model = mcnn()
    model.load_weights(path + 'pre_trained_models/' + test_sub + '.h5')

    # load four models

    prediction = model.predict(x_testcnn)
    conventional_overallPrediction += list(prediction.argmax(axis=1))
    groundTruth += list(y_test.argmax(axis=1))

    sub_labels = pd.concat([pd.Series(subjects), pd.Series(labels)], axis=1)
    train_cm = confusion_matrix(
        y_train.argmax(axis=1), model.predict(x_traincnn).argmax(axis=1), labels=[0, 1]
    )

    tm = np.array([
        [0.001, 0.999],
        [0.999, 0.001]
    ])

    means = np.array([0.0, 1.0])
    scales = np.array([1, 1])
    # scales = np.array([1, 1])
    dm = new_hsmm_get_duration(test_sub, sub_labels, samples)
    st = np.array([0.5, 0.5])

    h1 = HSMMModel(MultinomialEmissions(hsmm_get_emission_matrix(train_cm).T), dm, tm, st)

    temp_observations = prediction.argmax(axis=1).reshape(-1, )

    # observations = np.zeros_like(temp_observations, dtype='float64')
    observations = np.zeros_like(temp_observations, dtype='int')
    for idx, sta in enumerate(temp_observations):
        observations[idx] = sta

    new_prediction = h1.decode(observations)

    conventional_newOverallPrediction += list(new_prediction)

    print(classification_report(groundTruth, conventional_overallPrediction, digits=4))
    print(classification_report(groundTruth, conventional_newOverallPrediction, digits=4))
    print(accuracy_score(groundTruth, conventional_overallPrediction))
    print(accuracy_score(groundTruth, conventional_newOverallPrediction))

    print('Before Refinement: ')
    print(classification_report(groundTruth, conventional_overallPrediction, digits=4))
    print('After Refinement: ')
    print(classification_report(groundTruth, conventional_newOverallPrediction, digits=4))
    print()
    print('Before Refinement: ')
    print('ACC: ', accuracy_score(groundTruth, conventional_overallPrediction))
    print('After Refinement: ')
    print('ACC: ', accuracy_score(groundTruth, conventional_newOverallPrediction))
    print()
    print('Before Refinement: ')
    print('AUC: ', roc_auc_score(groundTruth, conventional_overallPrediction))
    print('After Refinement: ')
    print('AUC: ', roc_auc_score(groundTruth, conventional_newOverallPrediction))
